---
title: Matrix
subtitle: About the matrix platform.
comments: false
---

## Descripton

[Matrix](https://matrix.org)An open network for secure, decentralized communication.

## Alternatve services

[Slack](https://slack.com)

[Mattermost](https://mattermost.com)

IRC

## Clients

A full list of clients can be found at [matrix.org/clients/](https://matrix.org/clients/).

Clients I use and recomend are [Element](element.io/get-started) and [FluffyChat](fluffychat.im/)

## Homeservers

The most common matrix homeserver is the [matrix.org](https://app.element.io) homserver. However,
there are many more homeservers.

Some projects such as [ReactOS](https://reactos.org) and [ArchLinux](https://archlinux.org) have
their own matrix homeservers. If you are actively partipating in rooms on their homeservers, I would
encourage you to use their homeservers rather then the matrix.org homeserver.

You can also host your own homeserver. A list of homeservers can be found at [matrix.org/docs/projects/try-matrix-now/](https://matrix.org/docs/projects/try-matrix-now/).
