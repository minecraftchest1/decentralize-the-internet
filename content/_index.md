## Centerlization is an internet antipattern.

The internet is destributed by nature. However, more and more services
are centerlizing the internet. These include common sites like [Slack](https://slack.com), 
[Twitter](https://twitter), [Youtube](https://youtube.com), and others
are centerlizing the internet with vendor lockin, the inabbility to self
host, own your own data, and create compatible, simular services.

While we cannot fully remove the centerlization of services, we can try
to promote the use of decenterlizes and destributed alternatives to many
popular sites and services.

## Alternative sites and services

A list of centerlized, proprietary services and decenterlized or distributed
alternatives.

| Name | Alternatives | Categories |
|------|-------------|----------|
| [Slack](https://slack.com) | [Matrix](https://matrix.org) | Chat, Collaberation |
| [Twitter](https://twitter.com) | [Mastodon](https://joinmastodon.org/) | Microblogging, Collaberation |
| [Youtube](https://youtube.com) | [Peertube](https://joinpeertube.org/) | Videosharing |

**NOTE:** This is an incomplete list. I will add more over time. [https://fediverse.party/](https://fediverse.party/)
has a more complete list.
